<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>UVC Robotic</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="indexstyle.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kaushan+Script&family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=K2D:wght@300&display=swap" rel="stylesheet">

</head>
<body>

    <div id="home">
        <div class="home">
            <h1>UVC Robotic</h1>
            <p>หุ่นยนต์ฆ่าเชื้อด้วยรังสี <p1>UVC</p1></p>
        </div>
    </div>

    <div class="navbar">
        <a href="#home">Home</a>
        <a href="#result">Result</a>
        <a href="#howtouse">How to Use</a>
    </div>

    <div class="row">
        <div class="side">
            <h2>About Me</h2>
            <h5>เกี่ยวกับฉัน</h5>
            <br />
            <div class="fakeimg3" style="height:200px;"></div>
            <br />
            <p>&nbsp;&nbsp;&nbsp;&nbsp; หุ่นยนต์สามารถคำนวณระยะเวลาในการทำงานได้อัตโนมัติ นอกจากนี้ ผู้ใช้งานสามารถสั่งการเปิด - ปิดหลอดไฟ <p2>UVC</p2> ได้ผ่านทางเว็บไซต์ และมีการแจ้งเตือนการทำงานของหุ่นยนต์ทั้งก่อนและหลังการทำงาน</p>
            <br />
            <h2>Thank you</h2>
            <h5>ขอขอบคุณ</h5>
            <br />
            <div class="fakeimg" style="height:60px;">มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าพระนครเหนือ</div><br>
            <div class="fakeimg" style="height:60px;">บริษัท อาร์เอฟเอส จำกัด</div><br>
        </div>
        <div class="main">
            <div id="result">
                <div class="result">
                    <h2>Result</h2>
                    <h5>แจ้งเตือนการทำงาน</h5>
                    <br>
                    <div class="fakeimg4" style="height:200px;"></div>
                    <br>
                    <p>ควบคุมการเปิด-ปิดหลอดไฟ <p2>UVC</p2></p>
                    <br>
                    <form action="" method="POST">
                    <div class="button">
                        <button class="btn" type="submit" name = "ON">On</button>
                        <button class="btn1" type ="submit" name ="OFF"> Off </button>
                    </div>
                    <br />
                    <p>เวลาในการทำงาน</p>
                    <br>
<div class="timer">

                        <div class="show">                          

                            <button class="btn2" type="button" onclick="unhide()">
                                Timer
                                <div class="exp">
                                    <p class="and"><div id = "response"></div></p>
                                </div>
                            </button>
                        </div>
                        <br>                      

                    </div>
                    <br />
                    <p>แจ้งเตือนสถานะการทำงาน</p>
                    <br>
                    <div class="alert">
                        <div class="fakeimg2" style="height:60px;"><div>
                            <?php
if(isset($_POST['OFF']))
{					
echo"สวัสดีบิงซู";
}else{
    echo"สวัสดีเบลล่า";
}
?></div></div><br>
                    </div>
                </div>
                </div>
            </div>
        <div id="howtouse">
            <div class="howtouse">
                <h2>How to Use</h2>
                <h5>วิธีการใช้งานหุ่นยนต์ฆ่าเชื้อด้วยรังสี <p1>UVC</p1></h5>
                <br>
                <div class="fakeimg5" style="height:200px;"></div>
                <br>
                <p>&nbsp;&nbsp;&nbsp;&nbsp; 1. ทำการ <p2>Login</p2> เพื่อเข้าสู่ระบบของเว็บไซต์</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp; 2. กดสวิตช์เปิดเครื่องที่ตัวหุ่นยนต์ โดยจะมีเสียงร้องเตือนดังขึ้นเพื่อให้ผู้ใช้งานทราบว่าหุ่นยนต์พร้อมทำงาน</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp; 3. ผู้ใช้งานสามารถสั่งเปิดหลอดไฟ <p2>UVC</p2> ได้ผ่านทางปุ่มบนหน้าเว็บไซต์</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp; 4. หลังจากนั้นหุ่นยนต์จะเริ่มทำการคำนวณระยะเวลาที่เหมาะสมในการฉายรังสี <p2>UVC</p2> เพื่อฆ่าเชื้อไวรัสภายในห้อง เมื่อหุ่นยนต์เริ่มทำงานจะมีการแจ้งเตือนให้ทราบบนหน้าเว็บไซต์</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp; 5. เมื่อหุ่นยนต์ทำการฆ่าเชื้อไวรัสภายในห้องเรียบร้อยแล้วจะมีการแจ้งเตือนอีกครั้งบนหน้าเว็บไซต์และจะมีไฟ <p2>LED</p2> แสดงที่ตัวหุ่นยนต์เพื่อให้ทราบ</p>
                <br />
            </div>
        </div>
    </div>

    <div class="footer">
        <h2>KMUTNB x RFS</h2>
        <div class="logo"><img src="4.png" height="41px" width="331px"></div>
    </div>

</body>
</html>


<?php

$server 	= "localhost";
$username 	= "id15712742_uvuser";
$password 	= "^QuOu>b=&]P(Ol0Q";
$DB 		= "id15712742_uvrobot";
$update = new mysqli($server, $username, $password, $DB);
			if ($update->connect_error) {
				die("Connection failed: " . $update->connect_error);
			} 
			
			
if(isset($_POST['ON']))
{	
			
			$sql = "UPDATE status SET status = 1";
			if ($update->query($sql) === TRUE) {
            sleep(25);
            echo "<script> location.href='http://uvrobot.000webhostapp.com/uv_robot/first.php'; </script>";
            }
}

if(isset($_POST['OFF']))
{					
					
			$sql = "UPDATE status SET status = 0";
			if ($update->query($sql) === TRUE) {
			} 
	
	
}			
$conn = new mysqli($server, $username, $password, $DB);
$sql = "SELECT id, val, val2, date, time FROM test_nodemcu ORDER by id DESC LIMIT 1"; 
$result = $conn->query($sql);
if ($result->num_rows > 0) {
while($row = $result->fetch_assoc()) {
    //echo " &nbsp <strong>Distance:</strong> " . $row["val"]." &nbsp <strong>Date:</strong> " . $row["date"]." &nbsp <strong>Time:</strong>" .$row["time"]. "<p>".'<meta http-equiv="refresh" content="5" />';
}
}
else {
echo "0 results";
}
$conn->close();	

?>

<script type="text/javascript">
 var x = setInterval(fun1,1000);
 setTimeout('xx()',60000);
 function fun1(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET","response.php",false);
  xmlhttp.send(null);
  var str = document.getElementById("response").innerHTML=xmlhttp.responseText;

 }
 function xx(){
   clearInterval(x);
   exit();
  }
function unhide() {
var hid = document.getElementsByClassName("exp");
if (hid[0].offsetWidth > 0 && hid[0].offsetHeight > 0) {
hid[0].style.visibility = "visible";
}
}
</script>