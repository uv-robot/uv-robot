#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include "Wire.h"
#define WIFI_SSID "Benben"                                 
#define WIFI_PASSWORD "benten08"
#define SensorAddress byte(0x70)
#define RangeCommand byte(0x51)
#define ChangeAddressCommand1 byte(0xAA)
#define ChangeAddressCommand2 byte(0xA5)
int val = 1;
int m1in1 = D3;
int m1in2 = D5;
int ENA_m = D4;
int Relay = D8;
int max1 = 0;
int state1 = 0;
int LEDR = D6;
int a = 0;
int z = 0;
float v = 0;
int hr;
int mi;
float se;
int sw = 0;
int mi2 = 0;
int mi3 = 0;
unsigned long previousMillis = 0;
float sec = 0,m = 0,h = 0;
float Puvc = 10;
float L = 0.33;
float maxm = 0;
float alpha;
float E;
float E2;
int timer,timer1,timer2;
String sendval, postData;
void setup() {
pinMode(D7, OUTPUT);
pinMode(LED_BUILTIN, OUTPUT);
pinMode(D0,OUTPUT);
pinMode(m1in1,OUTPUT);
pinMode(m1in2,OUTPUT);
pinMode(ENA_m,OUTPUT);
pinMode(Relay,OUTPUT);   
pinMode(LEDR,OUTPUT);
Serial.begin(115200); 
Serial.println("Communication Started \n\n");  
Wire.begin(); 
WiFi.mode(WIFI_STA);           
WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
Serial.print("Connecting to ");
Serial.print(WIFI_SSID);
while (WiFi.status() != WL_CONNECTED) 
{ Serial.print(".");
    delay(500); }
Serial.println();
Serial.print("Connected to ");
Serial.println(WIFI_SSID);
Serial.print("IP Address is : ");
Serial.println(WiFi.localIP());
}

void loop() { 

HTTPClient http;
digitalWrite(D7,1);
unsigned long currentMillis = millis();
takeRangeReading();
int range = requestRange();
sendval = String(mi2);  
postData = "sendval=" + sendval;  
http.begin("http://uvcrobotic.000webhostapp.com/Getstatus.php");
http.addHeader("Content-Type", "application/x-www-form-urlencoded");
int httpCode = http.POST(postData);
int httpCode2 = http.GET();
String payload=http.getString();
  if(payload == "1")
  {
    if(sw == 0){
   digitalWrite(D0,HIGH);
   state1 = 1;
   sw = 1;
    }
  }
   else if (payload == "0")
   {
    digitalWrite(D0,LOW);
    digitalWrite(D7,1);
    state1 = 0;
    sw = 0;
    }
if (httpCode == 200) { Serial.println("Values uploaded successfully.");
String webpage = http.getString();
}
else { 
  Serial.println("Failed to upload values. \n"); 
  http.end(); 
  digitalWrite(D7,0);
  delay(1000);
  digitalWrite(D7,1);
  delay(1000);
  return;}

  if(state1 == 0){
    digitalWrite(m1in1,0);
    digitalWrite(m1in2,0);
    analogWrite(ENA_m,0);
    digitalWrite(Relay,0);
    digitalWrite(LEDR,0);
    digitalWrite(D7,1);  
    z = 0;
    alpha = 0;
    E = 0;
    E2 = 0;
    timer = 0;
    mi2 = 0;
    previousMillis = 0;
    sec = 0;
    m = 0;
    h = 0;
    v = 0;
    a = 0;
    range = 0;
    max1 = 0;
    maxm = 0;
    Serial.print(m);Serial.print(":");
    Serial.println(sec);
  }
    if(state1 == 1){
    if(currentMillis - previousMillis >= 1000) {
    digitalWrite(m1in1,0);
    digitalWrite(m1in2,0);
    analogWrite(ENA_m,255);
    digitalWrite(D7,1);
    delay(1000);
    digitalWrite(D7,0);
    delay(1000);
    a++;
    if(a == 3){
      state1 = 2;
    }
    previousMillis = currentMillis;
  }}
  if(state1 == 2){
    if(currentMillis - previousMillis >= 1000) {
    digitalWrite(m1in1,1);
    digitalWrite(m1in2,0);
    analogWrite(ENA_m,255);
    digitalWrite(D7,0);
    z++;
    if(z == 6 ){
      state1 = 3;
    }
    previousMillis = currentMillis;
  }
  if(range>max1){
  max1 = range;
  }}
  if(state1 == 3){
    if(currentMillis - previousMillis >= 1000) {
    digitalWrite(m1in1,0);
    digitalWrite(m1in2,1);
    analogWrite(ENA_m,255);
     digitalWrite(D7,1);
    z++;
    if(z == 13  ){
      state1 = 4;
    }
    previousMillis = currentMillis;
  }
  if(range>max1){
  max1 = range;
  }
  }
  if(state1 == 4){
    digitalWrite(m1in1,0); 
    digitalWrite(m1in2,0);
    analogWrite(ENA_m,0);
    Serial.print("Distance max: ");
    Serial.print(max1);
    Serial.println("   CM");
    maxm = (float)max1/100;
    Serial.print(maxm,3);
    Serial.println("   M");
    alpha = atan(L/(2*maxm));
    Serial.print("alpha: ");
    Serial.println(alpha,3);
    Serial.print("Puvc: ");
    Serial.println(Puvc,3);
    E = Puvc*((2*alpha)+sin(2*alpha))/(2*L*maxm*pow(PI,2));
    Serial.print("E: ");
    Serial.println(E);
    timer = 66/E;
    int hr = timer/3600;
    int mi = (timer%3600)/60;
    int se = timer%60;
    mi2 = mi+1;
    mi3 = mi2*60;
    timer1 = mi3-timer;
    timer2 = timer+timer1;
    Serial.print(hr);Serial.print(":");
    Serial.print(mi);Serial.print(":");
    Serial.println(se);
    Serial.println(timer2);
    delay(2000);
    state1 = 5; 
  }
  if(state1 == 5){
    if(currentMillis - previousMillis >= 1000) {
    digitalWrite(m1in1,0);
    digitalWrite(m1in2,0);
    analogWrite(ENA_m,0);
    digitalWrite(Relay,1);
    sec+=1.5;v+=1.5;
    if(v > 1 ){
      mi2 = 0;
    }
    if(sec >= 60){
      m++;
      sec = 0;
    }
    if(m >= 60){
      h++;
      m = 0;
    }
   if(h >= 24){
      h = 0;
    }
    if(v == timer2){
      state1 = 6;
    }
    previousMillis = currentMillis;
  }
  Serial.print(m);Serial.print(":");
  Serial.println(sec);
  }
  if(state1 == 6){
    digitalWrite(m1in1,0);
    digitalWrite(m1in2,0);
    analogWrite(ENA_m,255);
    digitalWrite(D7,1);
    digitalWrite(LEDR,1);
    delay(1000);
    digitalWrite(LEDR,0);
    delay(1000);
}}
void takeRangeReading(){
  Wire.beginTransmission(SensorAddress);
  Wire.write(RangeCommand);
  Wire.endTransmission();
}
int requestRange(){
  Wire.requestFrom(SensorAddress, byte(2));
  if(Wire.available() >= 2){
  byte HighByte = Wire.read();
  byte LowByte = Wire.read();
  int range = (HighByte, LowByte);
  return range;
}
else {
  return int(0);
}
}

void changeAddress(byte oldAddress, byte newAddress, boolean SevenBitHuh){
Wire.beginTransmission(oldAddress);
Wire.write(ChangeAddressCommand1); 
Wire.write(ChangeAddressCommand2); 
byte temp;
if(SevenBitHuh){ temp = newAddress << 1; }
else { temp = newAddress; }
Wire.write(temp); 
Wire.endTransmission();
}